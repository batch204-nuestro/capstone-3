import { useState, useEffect } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Link } from 'react-router-dom'

export default function SpecificProduct({match}) {

	// console.log(match)

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	//match.params holds the ID of our course in the courseId property
	const productId = match.params.productId;


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [])


const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
console.log(data._id)
      if(typeof data._id !== 'undefined'){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else{
        setUser({
          id: null,
          isAdmin: null
        })        
      }
    })

  }, [])


console.log(user.id)
	return(
		<Container className="mt-5">
			<Card>
				<Card.Body className="text-center">
					{/*<Card.Subtitle>Name:</Card.Subtitle>*/}
					<Card.Title>{name}</Card.Title>
					{/*<Card.Subtitle>Description:</Card.Subtitle>*/}
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>PhP {price}</Card.Text>
				</Card.Body>
			</Card>

			{(user.id === null) ?
		        	<Link className="nav-link text-center" to="/login" exact>Login to add to cart</Link>
		        	
		        	:
		        	<>
			        <Link className="nav-link text-center" to="/login" exact>Add to cart</Link>
		        	
		        	</>
		        }
		</Container>
	)
}
