import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function ProductCard({productProp}) {

	//console.log(props);
	// console.log(courseProp);

	const {_id, name, description, price} = productProp;

	
	
	return (
	    <Card className="mb-2">
	        <Card.Body>
	            <Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>PhP {price}</Card.Text>
{/*	            <Card.Text>Enrollees: {count}</Card.Text>
	            <Card.Text>Seats: {seats}</Card.Text>
	            <Button variant="primary" onClick={enroll}>Enroll</Button>*/}
	            <Link className="btn btn-primary" to={`/product/${_id}`}>Details</Link>
	        </Card.Body>
	    </Card>
	)
}