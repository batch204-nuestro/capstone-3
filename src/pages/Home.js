import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home(){

	const data = {
		title: "Ligwan Wild Honey Main Branch",
		content: "INTEGRITY is our business!",
		destination: "/products",
		label: "Browse Products!"
	}

	return(
		<>
			<Banner dataProp={data}/>
			<Highlights />
		</>
	)
}