import { useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard';
// import AdminView from '../components/AdminView';
import UserContext from '../UserContext';
import { add, total, get } from 'cart-localstorage' 

export default function Cart() {


function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== 'undefined'){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
        console.log(user.id)
      }else{
        setUser({
          id: null,
          isAdmin: null
        })        
      }
    })
  }, [])

	}


const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/order/pabili`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
		
		})
	}

	useEffect(() => {
		// console.log(process.env.REACT_APP_API_URL)
		// changes to env files are applied ONLY at build time (when starting the project locally)

		fetchData()
	}, [])





}